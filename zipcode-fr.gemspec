# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'zipcode-fr'
  s.version     = '1.3.1'
  s.licenses    = ['MIT']
  s.summary     = 'French zip codes and cities'
  s.description = <<-DESC
   Query city information by zip code and city name, indexed by word prefixes.
   Fork of https://github.com/lloeki/zipcode-fr
  DESC
  s.authors     = ['Custy France']
  s.email       = 'support-web@custy.com'
  s.files       = Dir['lib/**/*.rb'] + Dir['vendor/**/*']
  s.homepage    = 'https://gitlab.com/adhoc-gti/zipcode-fr'

  s.add_dependency 'zipcode-db', '~> 1.0'

  s.add_development_dependency 'minitest', '~> 5.10'
  s.add_development_dependency 'pry'
  s.add_development_dependency 'rake', '~> 12.1'
  s.add_development_dependency 'rubocop', '~> 0.81.0'
end
